<?php
/**
* Template Name: Template FullWidth
*
* @package Orion Music Hub [Mk.01]
* @subpackage orionmusichub-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="page-container page-inicio col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <?php the_content(); ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>

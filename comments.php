<div class="single-comments col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-0">
    <h2><?php _e('Comentarios', 'orionmusichub'); ?></h2>

    <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="3"></div>
    <hr>
    <?php comment_form(); ?>
    <?php wp_list_comments(); ?>
    <?php paginate_comments_links(); ?>

</div>

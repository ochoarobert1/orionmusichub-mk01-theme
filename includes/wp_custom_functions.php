<?php
/** FUNCION PARA COLOCAR TIEMPO EN ENTRADAS **/
function orionmusichub_time_ago() {
    global $post;
    $date = get_post_time('G', true, $post);
    $chunks = array(
        array( 60 * 60 * 24 * 365 , __( 'año', 'orionmusichub' ), __( 'años', 'orionmusichub' ) ),
        array( 60 * 60 * 24 * 30 , __( 'mes', 'orionmusichub' ), __( 'meses', 'orionmusichub' ) ),
        array( 60 * 60 * 24 * 7, __( 'semana', 'orionmusichub' ), __( 'semanas', 'orionmusichub' ) ),
        array( 60 * 60 * 24 , __( 'dia', 'orionmusichub' ), __( 'dias', 'orionmusichub' ) ),
        array( 60 * 60 , __( 'hora', 'orionmusichub' ), __( 'horas', 'orionmusichub' ) ),
        array( 60 , __( 'minuto', 'orionmusichub' ), __( 'minutos', 'orionmusichub' ) ),
        array( 1, __( 'segundo', 'orionmusichub' ), __( 'segundos', 'orionmusichub' ) )
    );
    if ( !is_numeric( $date ) ) {
        $time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
        $date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
        $date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
    }
    $current_time = current_time( 'mysql', $gmt = 0 );
    $newer_date = time( );
    $since = $newer_date - $date;
    if ( 0 > $since )
        return __(  ' un momento', 'orionmusichub' );
    for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        if ( ( $count = floor($since / $seconds) ) != 0 )
            break;
    }
    $output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
    if ( !(int)trim($output) ){
        $output = '0 ' . __( 'segundos', 'orionmusichub' );
    }
    return $output;
}

/* CUSTOM EXCERPT */
function get_excerpt($count){
    $foto = 0;
    $permalink = get_permalink($post->ID);
    $category = get_taxonomies($post->ID);
    $excerpt = get_the_excerpt($post->ID);
    if ($excerpt == ""){
        $excerpt = get_the_content($post->ID);
    }
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'... <a class="plus" href="'.$permalink.'">+</a>';
    return $excerpt;
}

/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function image_tag_class($class) {
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );

/* ADD CONTENT WIDTH FUNCTION */

if ( ! isset( $content_width ) ) $content_width = 1170;



// Creating the widget
/* CUSTOM MAILCHIMP FORM  */
class orionmusichub_custom_mailchimp_form extends WP_Widget {

    public function orionmusichub_custom_mailchimp_form_scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_media();
        wp_enqueue_script('admin-functions', get_template_directory_uri() . '/js/admin-functions.js', array('jquery'), null, true);
    }

    /* CONSTRUCT */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'custom_mailchimp_form',
            'description' => __('Este widget custom mostrará un formulario de Mailchimp personalizado, deberá agregarse la lista de Mailchimp al cual el user ingresará al momento de registrarse', 'orionmusichub')
        );
        parent::__construct( 'orionmusichub_custom_mailchimp_form', 'Formulario de Mailchimp', $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'orionmusichub_custom_mailchimp_form_scripts'));
    }

    /* OUTPUT */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $mailchimp_list = ! empty( $instance['mailchimp_list'] ) ? $instance['mailchimp_list'] : '';
        $file_download = ! empty( $instance['file_download'] ) ? $instance['file_download'] : '';
        $unique_formid = uniqid('mailchimp_form_');
        $unique_id = substr($unique_formid, 15);
        $unique_buttonid = 'mailchimp_button_' . substr($unique_formid, 15);
?>
<form id="<?php echo $unique_formid; ?>" class="mailchimp_form" type="POST">
    <div class="custom-form-control-container widget-custom-form-control-container">
        <input type="text" name="FNAME" id="FNAME" class="form-control" placeholder="<?php echo esc_html_e('Nombre', 'orionmusichub'); ?>" />
        <small class="danger d-none"></small>
        <input type="email" name="EMAIL" id="EMAIL" class="form-control" placeholder="<?php echo esc_html_e('Email', 'orionmusichub'); ?>" />
        <small class="danger d-none"></small>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="checkbox_1" name="checkbox_1" required>
            <label class="form-check-label" for="checkbox_1">
                <?php echo esc_html_e('Acepto la política de privacidad', 'orionmusichub'); ?>
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="checkbox_2" name="checkbox_2" required>
            <label class="form-check-label" for="checkbox_2">
                <?php echo esc_html_e('Acepto los términos y condiciones', 'orionmusichub'); ?>
            </label>
        </div>

        <input type="hidden" name="mailchimp-list" id="mailchimp-list" class="form-control" value="<?php echo $mailchimp_list; ?>" />
        <?php if ($file_download != '') { ?>
        <input type="hidden" name="file_download" id="file_download" class="form-control" value="<?php echo $file_download; ?>" />
        <?php } ?>
    </div>
    <button id="<?php echo $unique_buttonid; ?>" class="btn btn-md btn-submit">
        <?php _e('¡Descargar Gratis Ya!', 'orionmusichub'); ?></button>
    <div id="response_<?php echo $unique_id; ?>" class="form-response"></div>
</form>

<?php
        echo $args['after_widget'];
    }

    /* FORM */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
        $mailchimp_list = ! empty( $instance['mailchimp_list'] ) ? $instance['mailchimp_list'] : '';
        $file_download = ! empty( $instance['file_download'] ) ? $instance['file_download'] : '';
?>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
        <?php esc_attr_e( 'Título:', 'orionmusichub' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    <small>
        <?php _e('Título del Widget', 'orionmusichub'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'list' ) ); ?>">
        <?php esc_attr_e( 'Lista de Mailchimp:', 'orionmusichub' ); ?></label>
    <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'mailchimp_list' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mailchimp_list' ) ); ?>" type="text" value="<?php echo esc_attr( $mailchimp_list ); ?>">
    <small>
        <?php _e('Aquí podrá agregar el ListID de Mailchimp a donde debe llegar el correo del suscriptor', 'orionmusichub'); ?></small>
</p>
<p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'file_download' ) ); ?>">
        <?php esc_attr_e( 'Archivo a descargar al suscribirse: (Opcional)', 'orionmusichub' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'file_download' ); ?>" name="<?php echo $this->get_field_name( 'file_download' ); ?>" value="<?php echo esc_attr( $file_download ); ?>" />
    <small>
        <?php _e('Opcional: Agregue acá el archivo que el suscriptor ha ganado por suscribirse a Mailchimp', 'orionmusichub'); ?></small>

    <button class="upload_image_button button button-primary" style="display: block; margin-top: .3rem;">
        <?php _e('Cargar Archivo', 'orionmusichub'); ?></button>
</p>
<?php
    }

    /* UPDATE */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['mailchimp_list'] = ( ! empty( $new_instance['mailchimp_list'] ) ) ? sanitize_text_field( $new_instance['mailchimp_list'] ) : '';
        $instance['file_download'] = ( ! empty( $new_instance['file_download'] ) ) ? esc_url_raw( $new_instance['file_download'] ) : '';
        return $instance;
    }
}

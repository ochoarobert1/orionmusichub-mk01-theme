<?php
class VCExtendAddonClass {

    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrateWithVC' ) );
        // Use this when creating a shortcode addon
        add_shortcode( 'custom_blog_grid', array( $this, 'render_custom_grid' ) );
        add_shortcode( 'custom_form_creator', array( $this, 'render_custom_form_creator' ) );
        add_shortcode( 'custom_slider_grid', array( $this, 'render_custom_slider_grid' ) );
        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'loadCssAndJs' ) );
    }

    public function integrateWithVC() {
        // Check if WPBakery Page Builder is installed
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            // Display notice that Extend WPBakery Page Builder is required
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }

        /*
        Add your WPBakery Page Builder logic here.
        Lets call vc_map function to "register" our custom shortcode within WPBakery Page Builder interface.

        More info: http://kb.wpbakery.com/index.php?title=Vc_map
        */

        vc_map( array(
            "name" => __("Slider de Imágenes personalizado", 'orionmusichub'),
            "description" => __("Shortcode para un slider de 3-4 items", 'orionmusichub'),
            "base" => "custom_slider_grid",
            "class" => "",
            "controls" => "full",
            "icon" => get_template_directory_uri() . '/images/logo-composer.png', // or css class name which you can reffer in your css file later. Example: "orionmusichub_my_class"
            "category" => __('Content', 'js_composer'),
            //'admin_enqueue_js' => array(plugins_url('assets/orionmusichub.js', __FILE__)), // This will load js file in the VC backend editor
            //'admin_enqueue_css' => array(plugins_url('assets/orionmusichub_admin.css', __FILE__)), // This will load css file in the VC backend editor
            "params" => array(
                array(
                    "type" => "attach_images",
                    "class" => "",
                    'admin_label' => true,
                    "heading" => __('Imágenes del Slider', 'orionmusichub'),
                    "param_name" => "slider_items",
                    "value" => '0',
                    "description" => __("Imágenes del Slider a mostrar.", 'orionmusichub')
                )
            )
        )  );


        vc_map( array(
            "name" => __("Grilla de Posts Personalizada", 'orionmusichub'),
            "description" => __("Shortcode para la grilla personalizada del Home", 'orionmusichub'),
            "base" => "custom_blog_grid",
            "class" => "",
            "controls" => "full",
            "icon" => get_template_directory_uri() . '/images/logo-composer.png', // or css class name which you can reffer in your css file later. Example: "orionmusichub_my_class"
            "category" => __('Content', 'js_composer'),
            //'admin_enqueue_js' => array(plugins_url('assets/orionmusichub.js', __FILE__)), // This will load js file in the VC backend editor
            //'admin_enqueue_css' => array(plugins_url('assets/orionmusichub_admin.css', __FILE__)), // This will load css file in the VC backend editor
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    'admin_label' => true,
                    "heading" => __('Cantidad de Entradas', 'orionmusichub'),
                    "param_name" => "entry_quantity",
                    "value" => '0',
                    "description" => __("Cantidad de Entradas.", 'orionmusichub')
                )
            )
        )  );

        vc_map( array(
            "name" => __("Formulario de Mailchimp Customizable", 'orionmusichub'),
            "description" => __("Shortcode para insertar un formulario generado por MailChimp", 'orionmusichub'),
            "base" => "custom_form_creator",
            "class" => "",
            "controls" => "full",
            "icon" => get_template_directory_uri() . '/images/logo-composer.png', // or css class name which you can reffer in your css file later. Example: "orionmusichub_my_class"
            "category" => __('Content', 'js_composer'),
            //'admin_enqueue_js' => array(plugins_url('assets/orionmusichub.js', __FILE__)), // This will load js file in the VC backend editor
            //'admin_enqueue_css' => array(plugins_url('assets/orionmusichub_admin.css', __FILE__)), // This will load css file in the VC backend editor
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    'admin_label' => true,
                    "heading" => __('Lista de MailChimp', 'orionmusichub'),
                    "param_name" => "mailchimp_list",
                    "value" => '',
                    "description" => __("Lista de MailChimp.", 'orionmusichub')
                ),
                array(
                    "type" => "textarea_html",
                    "holder" => "div",
                    "class" => "",
                    "heading" => __('Texto previo al boton de Enviar', 'orionmusichub'),
                    "param_name" => "content",
                    "value" => '',
                    "description" => __("Texto previo al boton de Enviar.", 'orionmusichub')
                ),
                array(
                    "type" => "checkbox",
                    "class" => "",
                    "heading" => __('Colocar el checkbox de Política de Privacidad', 'orionmusichub'),
                    "param_name" => "checkbox_privacy",
                    "value" => '',
                    "description" => __("Colocar el checkbox de Política de Privacidad.", 'orionmusichub')
                ),
                array(
                    "type" => "vc_link",
                    "class" => "",
                    "heading" => __('Link de Política de Privacidad', 'orionmusichub'),
                    "param_name" => "checkbox_link",
                    "value" => '',
                    "description" => __("Colocar el Link de Política de Privacidad.", 'orionmusichub')
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    'admin_label' => true,
                    "heading" => __('Texto del Boton de Submit', 'orionmusichub'),
                    "param_name" => "submit_text",
                    "value" => '',
                    "description" => __("Texto del Boton de Submit.", 'orionmusichub')
                ),
            )
        )  );

    }

    /*
    Shortcode logic how it should be rendered
    */
    public function render_custom_slider_grid( $atts, $content = null ) {
        extract( shortcode_atts( array( 'slider_items' => 'slider_items' ), $atts ) );
        $slider_array = explode(',', $slider_items);
        $unique_formid = uniqid('slider_clients_');

        $output .= '<div id="'. $unique_formid .'" class="slider-clientes owl-carousel owl-theme">';
        foreach ($slider_array as $slider_item){
            $output .= '<div class="item">';
            $output .= wp_get_attachment_image( $slider_item, 'slider_img', false, array('class' => 'img-fluid') );
            $output .= '</div>';
        }
        $output .= '</div>';

        return $output;
    }

    public function render_custom_grid( $atts, $content = null ) {
        extract( shortcode_atts( array( 'entry_quantity' => 'entry_quantity' ), $atts ) );
        $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
        $quantity = absint( $entry_quantity );

        $output .= "<div class='container p-0'><div class='row no-gutters'><div class='custom-blog-grid-container col-12'>";
        $args = array('post_type' => 'post', 'posts_per_page' => $quantity, 'order' => 'DESC', 'orderby' => 'date');
        query_posts($args);
        $i = 1;
        while (have_posts()) : the_post();
        if ($i%2 == 0) { $class = "even"; } else { $class = "odd"; }
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'blog_img');
        $output .= "<div class='custom-blog-item col-12 " . $class ."'>";
        $output .= "<picture><img src='" . $featured_img_url . "' class='img-fluid img-blog-img'/></picture>";
        $output .= "<header><h2>". get_the_title() . "</h2></header>";
        $output .= "<p>". get_the_excerpt() . "</p>";
        $output .= "<a href='" . get_permalink() . "' title='" .  __('Leer Más', 'orionmusichub'). "' class='btn btn-md btn-blog'>". __('Leer Más', 'orionmusichub') . "</a>";
        $output .= "</div>";
        $i++; endwhile;
        wp_reset_query();
        $output .= "</div></div></div>";



        return $output;
    }

    public function render_custom_form_creator( $atts, $content ) {
        extract( shortcode_atts( array(
            'mailchimp_list' => 'mailchimp_list',
            'checkbox_privacy' => 'checkbox_privacy',
            'checkbox_link' => 'checkbox_link',
            'submit_text' => 'submit_text'
        ), $atts ) );
        $content = wpb_js_remove_wpautop($content, true);

        $href = vc_build_link( $checkbox_link );

        $unique_formid = uniqid('mailchimp_form_');
        $unique_id = substr($unique_formid, 15);
        $unique_buttonid = 'mailchimp_button_' . substr($unique_formid, 15);

        $output .= '<form id="'. $unique_formid .'" class="mailchimp_form_jscomposer" type="POST">';
        $output .= '<div class="custom-form-control-container">';
        $output .= '<input type="text" name="FNAME" id="FNAME" class="form-control" placeholder="' . esc_html('Nombre', 'orionmusichub') . '" required/>';
        $output .= '<input type="email" name="EMAIL" id="EMAIL" class="form-control" placeholder="' . esc_html('Email', 'orionmusichub') . '" required/>';
        $output .= '<input type="hidden" name="mailchimp-list" id="mailchimp-list" class="form-control" value="'. $mailchimp_list .'"/>';
        $output .= '</div>';
        $output .= '<div class="custom-mailchimp-form-content">';
        $output .= $content;
        $output .= '</div>';
        $output .= '<div class="custom-mailchimp-form-checkbox">';
        $output .= '<input type="checkbox" name="policy" id="policy" class="form-control" required/>';
        $output .= esc_html('Acepto la', 'orionmusichub') . ' <a href="'. $href["url"] .'" target="'. $href["target"] .'">' . esc_html('Política de Privacidad', 'orionmusichub') . '</a>';
        $output .= '</div>';
        $output .= '<div class="custom-mailchimp-form-checkbox">';
        $output .= '<input type="checkbox" name="newsletter" id="newsletter" class="form-control" required/>';
        $output .= esc_html('Acepto recibir el Newsletter.', 'orionmusichub');
        $output .= '</div>';
        $output .= '<button id="'. $unique_buttonid. '" class="btn btn-md btn-submit">' . $submit_text . '</button>';
        $output .= '<div id="response_' . $unique_id . '" class="form-response"></div>';
        $output .= '</form>';

        return $output;
    }

    /* Load plugin css and javascript files which you may need on front end of your site */
    public function loadCssAndJs() {
        wp_register_style( 'orionmusichub_jscomposer_style', get_template_directory_uri() . '/css/custom_jscomposer_orionmusichub.css');
        wp_enqueue_style( 'orionmusichub_jscomposer_style' );

        // If you need any javascript files on front end, here is how you can load them.
        wp_enqueue_script( 'orionmusichub_jscomposer_js', get_template_directory_uri() . '/js/custom_jscomposer_orionmusichub.js', array('jquery', 'main-functions'), NULL, true );

        wp_localize_script( 'orionmusichub_jscomposer_js', 'admin_url',
                           array(
                               'ajax_url' => admin_url('admin-ajax.php')
                           )
                          );
    }
}
// Finally initialize code
new VCExtendAddonClass();

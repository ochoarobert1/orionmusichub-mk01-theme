jQuery('.mailchimp_form_jscomposer').on('submit', function(event) {
    "use strict";
    event.preventDefault();
    var longid = jQuery(this).attr('id');
    var shortid = longid.split('_');

    jQuery.ajax({
        type: 'POST',
        url: admin_url.ajax_url,
        data: {
            action: 'orionmusichub_add_susbscriber',
            info: jQuery('#' + longid).serialize(),
        },
        beforeSend: function () {
            jQuery('#response_' + shortid[2]).html('<div id="loader"><span class="dot dot_1"></span> <span class="dot dot_2"></span> <span class="dot dot_3"></span> <span class="dot dot_4"></span></div>');
        },
        success: function (response) {
            jQuery('#response_' + shortid[2]).html('<div class="form-response-wrapper">' + response + '</div>');
        },
        error: function (request, status, error) {
            console.log(error);
        }
    });
});

function load_posts() {
    "use strict";
    var id_post = jQuery( ".blog-page-ajax-container > article:last" ).attr('id');
    jQuery.ajax({
        url : orionAjax.ajax_url,
        type : 'post',
        data : {
            id_post : id_post,
            action : 'ajax_posts'
        },
        beforeSend : function () {
            jQuery('.blog-page-ajax-container').append('<div id="loader"><span class="dot dot_1"></span> <span class="dot dot_2"></span> <span class="dot dot_3"></span> <span class="dot dot_4"></span></div>');
        },
        success : function (response) {
            jQuery('#loader').remove();
            jQuery('.blog-page-ajax-container').append(response);
        }
    });
}

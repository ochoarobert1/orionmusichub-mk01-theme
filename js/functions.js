var scrollHeight = 0,
    lastScrollTop = 0,
    st = jQuery(document).scrollTop(),
    secondaryNav = jQuery('.home-menu-section-content'),
    secondaryNavTopPosition = secondaryNav.offset().top,
    flag = false,
    currentQuiz = 1,
    lastQuiz = 0,
    quizHandler = '',
    quizIdHandler = '',
    lastQuizHandler = '',
    input_checked = false,
    passd = true,
    testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

jQuery(document).ready(function ($) {
    "use strict";
    jQuery(window).on("scroll", function () {
        var st = $(this).scrollTop();
        st > lastScrollTop ? $(".floating-nav").addClass("is-hidden") : $(window).scrollTop() > 200 ? ($(".floating-nav").removeClass("is-hidden"), setTimeout(function () {}, 200)) : $(".floating-nav").addClass("is-hidden"), lastScrollTop = st, 0 == $(this).scrollTop() && $(".floating-nav").addClass("is-hidden");
    });
    jQuery('.custom-select-container').on("click", "span", function (event) {
        jQuery('.custom-select-container span').removeClass('selected');
        jQuery(this).addClass('selected');
    });

    if (jQuery(".slider-clientes").length > 0) {
        var uniqueID_complete = jQuery(".slider-clientes").attr('id');
        jQuery('#' + uniqueID_complete).owlCarousel({
            items: 4,
            nav: false,
            loop: false,
            rewind: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 2000
        });
    }

    jQuery('.mailchimp_form').on('submit', function (event) {
        event.preventDefault();
        passd = true;
        var longid = jQuery(this).attr('id');
        var shortid = longid.split('_');

        var $el = jQuery('#FNAME');
        if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
            $el.next("small").removeClass("d-none");
            $el.next("small").html("El campo no puede estar vacio ni tener menos de dos caracteres");
            passd = false;
        } else {
            if (!$el.val().match(/^[a-zA-Z ]+$/)) {
                $el.next("small").removeClass("d-none");
                $el.next("small").html("<strong>Error:</strong>Solo se permiten caracteres alfabéticos");
                passd = false;
            } else {
                $el.next("small").addClass("d-none");
            }
        }

        var $el = jQuery('#EMAIL');
        if (($el.val() == '') || ($el.val() == null) || ($el.val().length < 2)) {
            $el.next("small").removeClass("d-none");
            $el.next("small").html("El campo no puede estar vacio ni tener menos de dos caracteres");
            passd = false;
        } else {
            if (testEmail.test($el.val())) {
                $el.next("small").addClass("d-none");
            } else {
                $el.next("small").removeClass("d-none");
                $el.next("small").html("El correo es invalido. recuerde que debe colocar un correo válido");
                passd = false;
            }

        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: admin_url.ajax_url,
                data: {
                    action: 'orionmusichub_custom_ajax_form_add_susbscriber',
                    info: jQuery('#' + longid).serialize(),
                },
                beforeSend: function () {
                    jQuery('#response_' + shortid[2]).html('<div id="loader"><span class="dot dot_1"></span> <span class="dot dot_2"></span> <span class="dot dot_3"></span> <span class="dot dot_4"></span></div>');
                },
                success: function (data) {
                    var respuesta = jQuery.parseJSON(data);
                    jQuery('#response_' + shortid[2]).html('<div class="form-response">' + respuesta['message'] + '</div>');
                    if (respuesta['send_file'] === true) {
                        SaveToDisk(jQuery('#file_download').val(), 'Descarga');
                    }
                },
                error: function (request, status, error) {
                    console.log(error);
                }
            });
        }
        passd == false;
    });


}); /* end of as page load scripts */

var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: true, // trigger animations on mobile devices (default is true)
    live: true, // act on asynchronously loaded content (default is true)
    callback: function (box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
});
wow.init();

function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.download = fileName || 'unknown';
        save.style = 'display:none;opacity:0;color:transparent;';
        (document.body || document.documentElement).appendChild(save);

        if (typeof save.click === 'function') {
            save.click();
        } else {
            save.target = '_blank';
            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
        }

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }

    // for IE
    else if (!!window.ActiveXObject && document.execCommand) {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}

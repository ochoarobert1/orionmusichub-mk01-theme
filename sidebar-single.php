<?php if ( is_active_sidebar( 'single_sidebar' ) ) : ?>
<ul class="single-sidebar" id="sidebar">
    <?php dynamic_sidebar( 'single_sidebar' ); ?>
</ul>
<?php endif; ?>


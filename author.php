<?php get_header(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <div class="banner-page-container col-12">
            <div class="banner-contents">
                <h1>
                    <?php _e('Blog', 'orionmusichub')?>
                </h1>
                <div class="blog-desc-container">
                    <?php _e('Los recursos de branding musical más completos para artistas y emprendedores musicales', 'orionmusichub'); ?>
                </div>
                <div class="blog-separator-line"></div>
            </div>
        </div>
        <div class="blog-page-container col-12">
            <div class="container">
                <div class="row">
                    <div class="blog-page-content col-8">
                        <?php if (have_posts()) : ?>
                        <div class="container">
                            <div class="blog-page-ajax-container row">
                                <?php while (have_posts()) : the_post(); ?>
                                <article id="<?php echo get_the_ID(); ?>" class="blog-page-item col-12 col-sm-6 col-md-6 col-xl-6 wow fadeIn">
                                    <picture>
                                        <div class="picture-overlay">
                                            <a href="<?php the_permalink(); ?>" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
                                                <i class="fa fa-plus-circle"></i>
                                            </a>
                                        </div>
                                        <a href="<?php the_permalink(); ?>" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
                                            <?php the_post_thumbnail('blog_img', array('class' => 'img-fluid img-blog-item')); ?>
                                        </a>
                                    </picture>
                                    <a href="<?php the_permalink(); ?>" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
                                        <h2>
                                            <?php the_title(); ?>
                                        </h2>
                                    </a>
                                    <p>
                                        <?php the_excerpt(); ?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-md btn-blog" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
                                        <?php _e('Leer Más', 'orionmusichub'); ?>
                                    </a>
                                </article>
                                <?php endwhile; ?>

                            </div>
                            <div class="ajax-pagination"><i onclick="load_posts()" class="fa fa-plus-circle"></i></div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="the-sidebar col-4">
                        <?php get_sidebar('main_sidebar'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>

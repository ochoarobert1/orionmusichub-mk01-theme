
<?php /* POST FORMAT - AUDIO */ ?>

<article id="post-<?php the_ID(); ?>" class="the-single col-xl-8 col-md-8 col-sm-8 col-12 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">

    <header>
        <h1 itemprop="name"><i class="fa fa-volume-up" aria-hidden="true"></i> <?php the_title(); ?></h1>
        <div class="container p-0">
            <div class="row no-gutters">
                <div class="col-4">
                    <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                </div>
                <div class="col-8">
                    <span class="author"><?php _e('Autor', 'orionmusichub'); ?>: <?php the_author_posts_link(); ?></span>
                </div>
            </div>
        </div>
    </header>
    <?php if ( has_post_thumbnail()) : ?>
    <picture>
        <?php the_post_thumbnail('single_img', array('class' => 'img-fluid img-single-featured')); ?>
    </picture>
    <?php endif; ?>
    <div class="post-content" itemprop="articleBody">
        <?php the_content() ?>
        <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
        <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
        <meta itemprop="url" content="<?php the_permalink() ?>">
        <footer>
            <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'orionmusichub' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
        </footer>
    </div><!-- .post-content -->
    <div class="single-share col-12">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" title="<?php _e('Compartir en Facebook', 'orionmusichub'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/share?text=<?php echo get_the_title(); ?>&url=<?php echo get_permalink(); ?>" title="<?php _e('Compartir en Twitter', 'orionmusichub'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        <a href="https://api.whatsapp.com/send?text=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-whatsapp"></i></a>
        <a href="mailto:?&subject=<?php echo get_the_title(); ?>&body=<?php echo get_permalink(); ?>" title="<?php _e('Enviar por Correo Electrónico', 'orionmusichub'); ?>" target="_blank"><i class="fa fa-envelope"></i></a>
    </div>
    <div class="separator-custom"></div>
    <?php if ( comments_open() ) { comments_template(); } ?>
</article> <?php // end article ?>

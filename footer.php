<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-12">
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php echo get_bloginfo('name')?>" class="img-fluid img-footer" />
                    </div>
                    <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                    <div class="footer-item col">
                        <ul id="sidebar-footer">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                    <div class="footer-item col">
                        <ul id="sidebar-footer">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="footer-copy col-12">
            <h6>Orion Music Hub 2018 &copy;
                <?php _e('Todos los derechos reservados', 'orionmusichub'); ?>
            </h6>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
<script id="mcjs">
    ! function(c, h, i, m, p) {
        m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
    }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/830ae1723b4e3bc33e9b48da4/d824ec5b0a5ff6782098ae85d.js");
</script>
</body>

</html>

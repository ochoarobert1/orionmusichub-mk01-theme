<?php get_header(); ?>
<?php the_post(); ?>
<main class="container">
    <div class="row">
        <div class="single-main-container col-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <div class="row">
                <?php $defaultargs = array('class' => 'img-fluid'); ?>
                <?php /* GET THE POST FORMAT */ ?>
                <?php get_template_part( 'post-formats/format', get_post_format() ); ?>
                <aside class="the-sidebar col-xl-4 col-md-4 col-sm-4 d-none" role="complementary">
                    <?php get_sidebar('single'); ?>
                </aside>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>

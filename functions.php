<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue");
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.3.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.0.1', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', false, '3.3.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script( 'jquery-migrate', 'http://code.jquery.com/jquery-migrate-3.0.1.min.js', array('jquery'), '3.0.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */
require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/class-wp-bootstrap-navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');


/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

//require_once('includes/wp_woocommerce_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

require_once('includes/wp_jscomposer_extended.php');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'orionmusichub', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
                      'default-image' => '',    // background image default
                      'default-color' => '',    // background color default (dont add the #)
                      'wp-head-callback' => '_custom_background_cb',
                      'admin-head-callback' => '',
                      'admin-preview-callback' => ''
                  )
                 );

/* --------------------------------------------------------------
    ADD CUSTOM EDITOR STYLE
-------------------------------------------------------------- */
function orionmusichub_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'orionmusichub_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'orionmusichub' ),
    'footer_menu' => __( 'Menu Footer - Principal', 'orionmusichub' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'orionmusichub_widgets_init' );
function orionmusichub_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Home', 'orionmusichub' ),
        'id' => 'home_sidebar',
        'description' => __( 'Estos widgets estarán visibles solo en el home', 'orionmusichub' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'orionmusichub' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en las entradas y páginas del sitio', 'orionmusichub' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Single Sidebar', 'orionmusichub' ),
        'id' => 'single_sidebar',
        'description' => __( 'Estos widgets seran vistos en las noticias / posts', 'orionmusichub' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 2, array(
        'name'          => __('Pie de Página %d'),
        'id'            => 'sidebar_footer',
        'description'   => __( 'Sección de Pie de Página', 'orionmusichub' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    register_widget( 'orionmusichub_custom_mailchimp_form' );

}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_login_logo() {
    $version_remove = NULL;
    wp_register_style('wp-custom-login', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-custom-login');

}
add_action('login_head', 'custom_login_logo');

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'orionmusichub' );
        echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'orionmusichub' );
        echo '<a href="http://robertochoa.com.ve/?utm_source=footer_admin&utm_medium=link&utm_content=orionmusichub" target="_blank">Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 9999, 500, true);
    add_image_size('single_img', 636, 297, true );
    add_image_size('slider_img', 260, 210, false );
}

/* --------------------------------------------------------------
    FILTER BY ID
-------------------------------------------------------------- */
add_filter( 'posts_where', function( $where, $q ) {
    global $wpdb;

    if( $pid = $q->get( 'wpse_pid' ) )
    {
        // Get the compare input
        $cmp = $q->get( 'wpse_compare' );

        // Only valid compare strings allowed:
        $cmp = in_array(
            $cmp,
            [ '<', '>', '!=', '<>', '<=', '>=' ]
        )
            ? $cmp
            : '=';  // default

        // SQL part
        $where .= $wpdb->prepare( " AND {$wpdb->posts}.ID {$cmp} %d ", $pid ) ;
    }
    return $where;

}, 10, 2 );

/* --------------------------------------------------------------
    AJAX CALLER
-------------------------------------------------------------- */

add_action( 'wp_enqueue_scripts', 'ajax_orion_enqueue_scripts' );
function ajax_orion_enqueue_scripts() {
    wp_enqueue_script( 'orionAjax', get_template_directory_uri() . '/js/ajax-scripts.js', array('jquery'), '1.0', true );

    wp_localize_script( 'orionAjax', 'orionAjax', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}

add_action('wp_ajax_nopriv_ajax_posts','ajax_posts');
add_action('wp_ajax_ajax_posts','ajax_posts');

function ajax_posts()
{

    $id_post = $_POST['id_post'];

    $query = new WP_Query(
        [
            'wpse_pid'        => $id_post,    // Our custom post id argument
            'wpse_compare'    => '<',    // Out custom compare argument (<,>,<=,>=,!=,<>)
            'posts_per_page'  => 10,    // Modify this to your needs
        ]
    );

    if ($query->have_posts()) :
    while($query->have_posts()) : $query->the_post();
?>
<article id="<?php echo get_the_ID(); ?>" class="blog-page-item col-12 col-sm-6 col-md-6 col-xl-6 wow fadeIn">
    <picture>
        <a href="<?php the_permalink(); ?>" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
            <?php the_post_thumbnail('blog_img', array('class' => 'img-fluid img-blog-item')); ?>
        </a>
    </picture>
    <a href="<?php the_permalink(); ?>" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
        <h2>
            <?php the_title(); ?>
        </h2>
    </a>
    <p>
        <?php the_excerpt(); ?>
    </p>
    <a href="<?php the_permalink(); ?>" class="btn btn-md btn-blog" title="<?php _e('Leer Más', 'orionmusichub'); ?>">
        <?php _e('Leer Más', 'orionmusichub'); ?>
    </a>
</article>
<?php
    endwhile;
    endif;

    wp_die();
}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/* --------------------------------------------------------------
    CUSTOM FUNCTION - AJAX CALLER - ADD SUBSCRIBER
-------------------------------------------------------------- */

function orionmusichub_custom_ajax_form_add_susbscriber_callback() {

    $api_key = get_option('orionmusichub_mailchimp_api');

    $info = array();
    parse_str($_POST['info'], $info);

    $list_id = $info['mailchimp-list'];

    $nombre_largo = $info['FNAME'];
    $correo = htmlentities($info['EMAIL']);

    $nombre = explode(' ', $nombre_largo);

    $server = 'us18.';

    $auth = base64_encode( 'user:'.$api_key );

    $data = array(
        'apikey'        => $api_key,
        'email_address' => $correo,
        'status'        => 'pending',
        'merge_fields'  => array(
            'FNAME' => $nombre[0],
            'LNAME'    => $nombre[1]
        )
    );

    $json_data = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                               'Authorization: Basic '.$auth));
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

    $result = curl_exec($ch);

    $result_obj = json_decode($result);

    // printing the result obtained
    $status = $result_obj->status;

    if (($status == 'subscribed') || ($status == 'pending')) {
        if (isset($info['file_download'])) {
            $response['message'] = __('Se ha añadido correctamente el correo a la lista, en breve iniciará su descarga', 'orionmusichub');
        } else {
            $response['message'] = __('Se ha añadido correctamente el correo a la lista', 'orionmusichub');
        }
    }

    if ($status == 400) {
        if (isset($info['file_download'])) {
            $response['message'] = __('Ya su correo esta dentro de la lista, en breve iniciará su descarga', 'orionmusichub');
        } else {
            $response['message'] = __('Ya su correo esta dentro de la lista', 'orionmusichub');
        }
    }

    echo json_encode($response);
    die();
}

add_action('wp_ajax_nopriv_orionmusichub_custom_ajax_form_add_susbscriber', 'orionmusichub_custom_ajax_form_add_susbscriber_callback');
add_action('wp_ajax_orionmusichub_custom_ajax_form_add_susbscriber', 'orionmusichub_custom_ajax_form_add_susbscriber_callback');
